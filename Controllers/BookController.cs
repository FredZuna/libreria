﻿using libreria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace libreria.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index(string title, string dateEdition, string author)
        {

            try
            {
                using (var db = new LibraryEntities1())
                {
                    string conditional = "";

                    if (dateEdition != "" && dateEdition != null) {
                        conditional = " AND bk.dateEdition = '" + dateEdition + "'";
                    }

                    if (title != "" && title != null)
                    {
                        title = " AND bk.title LIKE '%" + title + "%'";
                    }

                    string sql = @"select 
                                DISTINCT bk.bookId,  bk.title, bk.dateEdition, dbo.getTotalAuthor(bk.bookId) as 'total'
                                from 
                                [dbo].[AuthorBook] as ab ,  [dbo].[Book] as bk
                                where 
                                ab.bookId = bk.bookId  " + conditional + " " + title;

                    List<BookCE> list = db.Database.SqlQuery<BookCE>(sql).ToList();
                    return View(list);
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "error to create a Loan -  " + ex.Message);
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            try
            {
                using (var db = new LibraryEntities1())
                {
                    Book book = db.Book.Where(b => b.bookId == id).FirstOrDefault();                    
                    List<Author> listAuthors = db.Author.ToList();

                    List<int> authorsSelected = db.AuthorBook.Where(ab => ab.bookId == book.bookId)
                                          .Select(b => b.authorId)
                                          .ToList();

                    BookAuthorCE bookAuthorCE = new BookAuthorCE();
                    bookAuthorCE.bookId = book.bookId;
                    bookAuthorCE.title = book.title;
                    bookAuthorCE.dateEdition = book.dateEdition;
                    bookAuthorCE.authorsSelected = authorsSelected;
                    bookAuthorCE.authors = listAuthors;

                    return View(bookAuthorCE);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "error to edit a book -  " + ex.Message);
                return View();
            }
        }


        [HttpPost]
        public ActionResult Edit(BookAuthorCE bookAuthorCE)
        {


            try
            {
                using (var db = new LibraryEntities1())
                {
                    // prepare book
                    Book book = db.Book.Find(bookAuthorCE.bookId);
                    book.title = bookAuthorCE.title;
                    book.dateEdition = bookAuthorCE.dateEdition;
                    //db.SaveChanges();


                    // remove authors of the book
                    var removeAuthors = (from authorBook in db.AuthorBook
                                  where authorBook.bookId== bookAuthorCE.bookId                                     
                                  select authorBook).ToList();

                    if (removeAuthors != null)
                    {
                        db.AuthorBook.RemoveRange(removeAuthors);                        
                    }
                    //db.SaveChanges();



                    foreach (var authorId in bookAuthorCE.authorsSelected)
                    {
                        AuthorBook authorBook = new AuthorBook();
                        authorBook.bookId = book.bookId;
                        authorBook.authorId = authorId;

                        db.AuthorBook.Add(authorBook);
                    }

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "error to create a book -  " + ex.Message);
                return View();
            }
        }


        public ActionResult AuthorBookList()
        {

            using (var db = new LibraryEntities1())
            {
                return PartialView(db.Author.ToList());
            }

        }



        public ActionResult Create()
        {
            using (var db = new LibraryEntities1()) {

                List<Author> listAuthors = db.Author.ToList();

                BookAuthorCE bookAuthorCE = new BookAuthorCE();
                bookAuthorCE.authors = listAuthors;
                return View(bookAuthorCE);
            }
        }

        [HttpPost]
        public ActionResult Create(BookAuthorCE bookAuthorCE)
        {
            try
            {
                using (var db = new LibraryEntities1())
                {
                    // prepare book
                    Book book = new Book();
                    book.title = bookAuthorCE.title;
                    book.dateEdition = bookAuthorCE.dateEdition;

                    db.Book.Add(book);                    

                    foreach (var authorId in bookAuthorCE.authorsSelected)
                    {
                        AuthorBook authorBook = new AuthorBook();
                        authorBook.bookId = book.bookId;
                        authorBook.authorId = authorId;

                        db.AuthorBook.Add(authorBook);
                    }

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "error to create a book -  " + ex.Message);
                return View();
            }
        }

        public ActionResult searchBook()
        {
            using (var db = new LibraryEntities1())
            {
                SearchBook searchBook = new SearchBook();                
                return View(searchBook);
            }
        }

        [HttpPost]
        public ActionResult searchBook(SearchBook searchBook)
        {
            try
            {
                using (var db = new LibraryEntities1())
                {
                    // prepare book
                    /*Book book = new Book();
                    book.title = bookAuthorCE.title;
                    book.dateEdition = bookAuthorCE.dateEdition;

                    db.Book.Add(book);

                    foreach (var authorId in bookAuthorCE.authorsSelected)
                    {
                        AuthorBook authorBook = new AuthorBook();
                        authorBook.bookId = book.bookId;
                        authorBook.authorId = authorId;

                        db.AuthorBook.Add(authorBook);
                    }

                    db.SaveChanges();*/
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "error to create a book -  " + ex.Message);
                return View();
            }
        }


    }
}