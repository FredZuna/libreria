﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace libreria.Models
{
    public class SearchBook
    {
        public string title { get; set; }
        public Nullable<System.DateTime> dateEdition { get; set; }
        public string author { get; set; }        
    }
}