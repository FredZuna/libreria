﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace libreria.Models
{
    public class BookAuthorCE
    {
        public int bookId { get; set; }
        [Required]
        public string title { get; set; }
        [Required]
        [DataType(DataType.Date, ErrorMessage = "Date only")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> dateEdition { get; set; }
        public List<Author> authors { get; set; }
        [Required]
        public List<int> authorsSelected { get; set; }
    }
}