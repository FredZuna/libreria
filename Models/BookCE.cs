﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace libreria.Models
{
    public class BookCE
    {
        public int bookId { get; set; }
        public string title { get; set; }
        public Nullable<System.DateTime> dateEdition { get; set; }
        public int total { get; set; }
    }
}